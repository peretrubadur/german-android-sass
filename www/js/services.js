angular.module('starter.services', [])

.factory('Grammar', function($http){
  return{
    getData : function() {
        return $http({
            url: 'https://public-api.wordpress.com/rest/v1.1/sites/yourdailygerman.wordpress.com/posts/?number=43&category="grammar-basics"&fields=ID, title,excerpt, date, content',
            method: 'GET'
        })
    }
 }
})
.factory('Categories', function($http){
  return{
    getData : function() {
        return $http({
            url: 'https://public-api.wordpress.com/rest/v1/sites/yourdailygerman.wordpress.com/categories/?fields=name, slug, post_count',
            method: 'GET'
        })
    }
 }
})
.factory('Data', function($http){
  return{
    get : function(url) {
        return $http({
            url: 'https://public-api.wordpress.com/rest/v1.1/sites/yourdailygerman.wordpress.com/posts/?category="'+url+'"&fields=ID, title,excerpt, date, content',
            method: 'GET'
        })
    }
 }
})

// $http.get(url)
//   .success(function(data, status) {
//       alert(status); // HTTP status code of the response
//   })
//   .error(function(data, status) {
//       alert('Error with status code: ' + status); 
//   });

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'https://pbs.twimg.com/profile_images/578237281384841216/R3ae1n61.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
